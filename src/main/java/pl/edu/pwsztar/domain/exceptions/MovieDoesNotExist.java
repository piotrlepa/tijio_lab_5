package pl.edu.pwsztar.domain.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "movie does not exist")
public class MovieDoesNotExist extends RuntimeException {
}
