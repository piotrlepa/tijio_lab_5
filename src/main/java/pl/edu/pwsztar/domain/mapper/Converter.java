package pl.edu.pwsztar.domain.mapper;

interface Converter<F, T> {
    T convert(F from);
}
